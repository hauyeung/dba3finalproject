create table fp_dimDate(
	date_key int primary key auto_increment,
	date date, 
	is_weekend tinyint(1), 
	year int,
	quarter int, 
	month int,
	day date
);

create table fp_dimEmployees(
	employee_key int primary key auto_increment,
	EmployeeID int, 
	LastName varchar(100), 
	FirstName varchar(100), 
	Title varchar(10), 
	TitleOfCourtesy varchar(10), 
	BirthDate datetime, 
	HireDate datetime, 
	Address varchar(200), 
	City varchar(100), 
	Region varchar(100), 
	PostalCode varchar(100), 
	Country varchar(100),
	HomePhone varchar(100), 
	Extension varchar(100)	
);

create table fp_dimCustomers(
	customer_key int primary key auto_increment,
	CustomerID int, 
	CompanyName varchar(200), 
	ContactName varchar(200), 
	ContactTitle varchar(20), 
	Address varchar(200), 
	City varchar(200), 
	Region varchar(200), 
	PostalCode varchar(200), 
	Country varchar(200), 
	Phone varchar(200), 
	Fax varchar(200),
	start_date datetime,
	end_date datetime
);

create table fp_dimSuppliers(
	supplier_key int primary key auto_increment,
	SupplierID int, 
	CompanyName varchar(200), 
	ContactName varchar(200), 
	ContactTitle varchar(200), 
	Address varchar(200), 
	City varchar(200), 
	Region varchar(200), 
	PostalCode varchar(20), 
	Country varchar(200), 
	Phone varchar(200), 
	Fax varchar(200), 
	HomePage text,
	start_date datetime,
	end_date datetime
);

create table fp_dimProducts(
	proudct_key int primary key auto_increment,	
	ProductID int, 
	ProductName varchar(200), 
	Discontinued varchar(200),
	CategoryName varchar(200),
	CategoryDescription text,
	start_date datetime,
	end_date datetime
);

create table fp_dimOrders(
	order_key int primary key auto_increment,
	OrderID int, 
	CustomerName varchar(200), 
	ContactName varchar(200),
	OrderDate varchar(200), 
	RequiredDate varchar(200), 
	ShipName varchar(200), 
	ShipAddress varchar(200), 
	ShipCity varchar(200),
	ShipRegion varchar(200), 
	ShipPostalCode varchar(20), 
	ShipCountry varchar(200)
);


create table fp_factOrderUnitPrice(	
	date_key int, 
	product_key int, 
	order_key int,
	UnitPrice decimal,
	run_id int
);

create table fp_factSupplierUnitPrice (
	date_key int, 
	product_key int, 
	supplier_key int,
	UnitPrice decimal,
	run_id int
);

create table stage_fp_factOrderUnitPrice(
	OrderID int, 
	ProductID int, 
	UnitPrice int, 
	OrderDate date
);

create table stage_fp_factSupplierUnitPrice(
	ProductID int, 
	SupplierID int, 
	UnitPrice decimal
);

delimiter //
create procedure etl_preFpFactOrderUnitPrice()
begin
alter table stage_fp_factOrderUnitPrice add index (OrderID);
alter table stage_fp_factOrderUnitPrice add index (ProductID);
truncate fp_factOrderUnitPrice;
end
//

delimiter //
create procedure etl_preFpFactSupplierUnitPrice()
begin
alter table stage_fp_factOrderUnitPrice add index (ProductID);
alter table stage_fp_factOrderUnitPrice add index (OrderId);
truncate fp_factSupplierUnitPrice;
end
//

alter table fp_dimEmployees add index (EmployeeId);
alter table fp_dimCustomers add index (CustomerID);
alter table fp_dimSuppliers add index (SupplierID);
alter table fp_dimProducts add index (ProductID);
alter table fp_dimOrders add index (OrderID);
alter table fp_factOrderUnitPrice add index (OrderId);
alter table fp_factOrderUnitPrice add index (ProductId);
alter table fp_factSupplierUnitPrice add index (OrderId);
alter table fp_factSupplierUnitPrice add index (SupplierId);
